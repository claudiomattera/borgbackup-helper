Borgbackup Helper
====

A [Rust] application to simplify backups with [BorgBackup].

<https://gitlab.com/claudiomattera/borgbackup-helper/>

[BorgBackup] is an open-source command-line tool to perform incremental, deduplicated, and compressed backups.
Calling BorgBackup from command line and passing all required arguments can be tedious and error prone.
Borgbackup-helper, on the other hand, reads its configuration from a file, and uses it to call BorgBackup with the appropriate arguments.

Borgbackup-helper is inspired to [borgmatic], another program that can be used to automate backups with BorgBackup.
In facts, it follows the same format for configuration files.

[BorgBackup]: https://www.borgbackup.org/
[borgmatic]: https://torsion.org/borgmatic/


Installation
----

Executables for Linux and Windows can be found in the [releases page](https://gitlab.com/claudiomattera/borgbackup-helper/-/releases).


### From source

This application can be compiled using the Rust toolchain.

~~~~shell
cargo build --release
~~~~

The resulting executable will be created in `target/release/borgbackup-helper`.


Usage
----

When run from command-line, the configuration file is read and BorgBackup is used to create and prune backups.

~~~~plain
> borgbackup-helper -v --configuration path/to/configuration.yaml
[INFO  borgbackup_helper] Creating backups to 1 repositories
Creating archive at "borg_repository:repo::{user}@{hostname}-{utcnow:%Y-%m-%dT%H:%M:00}Z"
------------------------------------------------------------------------------
Archive name: user@hostname-2020-01-01T00:00:00Z
Archive fingerprint: abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd
Time (start): Wed, 2020-01-01 20:00:00
Time (end):   Wed, 2020-01-01 20:01:00
Duration: 60 seconds
Number of files: 7634
Utilization of max. archive size: 0%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:               15.00 GB             10.00 GB              4.32 MB
All archives:              500.00 GB            250.00 GB             12.42 GB

                       Unique chunks         Total chunks
Chunk index:                  234432              7694587
------------------------------------------------------------------------------
[INFO  borgbackup_helper] Pruning backups to 1 repositories
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
Deleted data:              -15.00 GB            -10.00 GB             -4.33 MB
All archives:              500.00 GB            250.00 GB             12.42 GB

                       Unique chunks         Total chunks
Chunk index:                  134343              5353466
------------------------------------------------------------------------------
~~~~

Password for BorgBackup will be read from standard input, otherwise it can be supplied through the environment variable `BORG_PASSPHRASE`.


~~~~plain
> borgbackup-helper --help
borgbackup-helper 0.1.0
Claudio Mattera <claudio@mattera.it>
An application to simplify backups with BorgBackup

USAGE:
    borgbackup-helper [FLAGS] --configuration <PATH>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Verbosity level

OPTIONS:
    -c, --configuration <PATH>    Path to configuration file
~~~~


Configuration
----

The configuration is read from a [YAML]-formatted file following the same structure of [borgmatic's configuration](https://torsion.org/borgmatic/docs/reference/configuration/).
However, some borgmatic's features are not supported and must be commented out in the configuration file.

[YAML]: https://yaml.org/


License
----

Copyright Claudio Mattera 2020

You are free to copy, modify, and distribute this application with attribution under the terms of the [MIT license]. See the [`License.txt`](./License.txt) file for details.

[Rust]: https://rust-lang.org/
[MIT license]: https://opensource.org/licenses/MIT
