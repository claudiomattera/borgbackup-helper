stages:
  - configure
  - lint
  - build
  - test
  - documentation
  - release
  - package
  - publish


# Ignore warnings about format from rustfmt
.lint:rustfmt:
  stage: lint
  image: rust:latest
  before_script:
    - rustup component add rustfmt
  script:
    - cargo fmt --all -- --check
  allow_failure: true


lint:clippy:
  stage: lint
  image: rust:latest
  before_script:
    - rustup component add clippy
  script:
    - cargo clippy --all-targets --all-features
  allow_failure: true


build:cargo:
  stage: build
  image: rust:latest
  script:
    - cargo build --all-targets --all-features
  artifacts:
    paths:
      - 'target/debug/'
    expire_in: 1 day


test:cargo:
  stage: test
  image: rust:latest
  script:
    - cargo test
  dependencies:
    - build:cargo


# Skip documentation for non libraries
.documentation:cargo:
  stage: documentation
  image: rust:latest
  script:
    - cargo doc --no-deps
  dependencies:
    - build:cargo
  artifacts:
    paths:
      - 'target/doc/'
    expire_in: 1 day


release:cargo:linux:x86_64:
  stage: release
  image: rust:latest
  only:
    - tags
  before_script:
    - rustup target add x86_64-unknown-linux-gnu
  script:
    - cargo build --release --target=x86_64-unknown-linux-gnu
    - cp "target/x86_64-unknown-linux-gnu/release/${CI_PROJECT_NAME}" "${CI_PROJECT_NAME}-linux-x86_64"
  artifacts:
    paths:
      - "target/x86_64-unknown-linux-gnu/release/${CI_PROJECT_NAME}"
      - "${CI_PROJECT_NAME}-linux-x86_64"
    expire_in: 1 day


release:cargo:linux-musl:x86_64:
  stage: release
  image: rust:latest
  only:
    - tags
  before_script:
    - rustup target add x86_64-unknown-linux-musl
  script:
    - cargo build --release --target=x86_64-unknown-linux-musl
    - cp "target/x86_64-unknown-linux-musl/release/${CI_PROJECT_NAME}" "${CI_PROJECT_NAME}-linux-x86_64-standalone"
  artifacts:
    paths:
      - "${CI_PROJECT_NAME}-linux-x86_64-standalone"
    expire_in: 1 day


# Cross compilation to Windows x86 from Debian is unsupported
# https://github.com/rust-lang/rust/issues/12859
.release:cargo:windows:x86:
  stage: release
  image: rust:latest
  only:
    - tags
  before_script:
    - apt-get update
    - apt-get install -y mingw-w64
    - rustup target add i686-pc-windows-gnu
  script:
    - cargo build --release --target=i686-pc-windows-gnu
    - cp "target/i686-pc-windows-gnu/release/${CI_PROJECT_NAME}.exe" "${CI_PROJECT_NAME}-windows-x86.exe"
  artifacts:
    paths:
      - "${CI_PROJECT_NAME}-windows-x86.exe"
    expire_in: 1 day


release:cargo:windows:x86_64:
  stage: release
  image: rust:latest
  only:
    - tags
  before_script:
    - apt-get update
    - apt-get install -y mingw-w64
    - rustup target add x86_64-pc-windows-gnu
  script:
    - cargo build --release --target=x86_64-pc-windows-gnu
    - cp "target/x86_64-pc-windows-gnu/release/${CI_PROJECT_NAME}.exe" "${CI_PROJECT_NAME}-windows-x86_64.exe"
  artifacts:
    paths:
      - "${CI_PROJECT_NAME}-windows-x86_64.exe"
    expire_in: 1 day


package:ebuild:
  stage: package
  image: rust:latest
  only:
    - tags
  before_script:
    - apt-get update
    - apt-get install -y jq
  script:
    - FILTER='.packages | map(select(.name != "${CI_PROJECT_NAME}")) | sort_by(.name + .version) | .[] | select(.name != "borgbackup-helper") | (.name + "-" + .version)'
    - CRATES=`cargo metadata --all-features --format-version=1 | jq -r "${FILTER}"`
    - echo $CRATES | sed -e '/^{{ crates }}/{r /dev/stdin' -e 'd;}' packaging/gentoo/${CI_PROJECT_NAME}.ebuild > packaging/gentoo/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}.ebuild
  artifacts:
    paths:
      - 'packaging/gentoo/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}.ebuild'
    expire_in: 1 day


package:cargo:debian:x86_64:
  stage: package
  image: rust:latest
  only:
    - tags
  dependencies:
    - release:cargo:linux:x86_64
  before_script:
    - rustup target add x86_64-unknown-linux-gnu
    - cargo install cargo-deb
  script:
    - cargo deb --no-build --target=x86_64-unknown-linux-gnu
    - cp "target/x86_64-unknown-linux-gnu/debian/${CI_PROJECT_NAME}_${CI_COMMIT_TAG}_amd64.deb" "${CI_PROJECT_NAME}_${CI_COMMIT_TAG}_amd64.deb"
  artifacts:
    paths:
      - "${CI_PROJECT_NAME}_${CI_COMMIT_TAG}_amd64.deb"
    expire_in: 1 day



publish:gitlab:
  stage: publish
  image: claudiomattera/gitlab-release:0.1.2
  only:
    - /^\d+(\.\d+)*$/
  script:
    - gitlab-release --name "Version ${CI_COMMIT_TAG}" "${CI_PROJECT_NAME}-linux-x86_64" "${CI_PROJECT_NAME}-linux-x86_64-standalone" "${CI_PROJECT_NAME}-windows-x86_64.exe" "${CI_PROJECT_NAME}_${CI_COMMIT_TAG}_amd64.deb"
