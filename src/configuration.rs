// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use ::serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Configuration {
    pub location: Location,
    pub retention: Retention,
    pub storage: Storage,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Location {
    pub source_directories: Vec<String>,
    pub repositories: Vec<String>,
    pub one_file_system: bool,
    pub exclude_patterns: Vec<String>,
    pub exclude_caches: bool,
    pub exclude_if_present: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Retention {
    pub keep_within: Option<String>,
    pub keep_secondly: Option<i64>,
    pub keep_minutely: Option<i64>,
    pub keep_hourly: Option<i64>,
    pub keep_daily: Option<i64>,
    pub keep_weekly: Option<i64>,
    pub keep_monthly: Option<i64>,
    pub keep_yearly: Option<i64>,

    #[serde(default = "default_prefix")]
    pub prefix: String,
}

fn default_prefix() -> String {
    "{hostname}-".to_owned()
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Storage {
    #[serde(default = "default_archive_name_format")]
    pub archive_name_format: String,
}

fn default_archive_name_format() -> String {
    "{hostname}-{now:%Y-%m-%dT%H:%M:%S.%f}".to_owned()
}
