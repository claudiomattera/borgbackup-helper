// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use ::log::*;

use ::env_logger;

use ::std::fs::File;
use ::std::process::exit;

use ::anyhow::anyhow;
use ::anyhow::{Context, Result};

use ::clap::{app_from_crate, crate_authors, crate_description, crate_name, crate_version};
use ::clap::{Arg, ArgMatches};

use ::human_panic::setup_panic;

mod borg;
mod configuration;

use crate::borg::{build_create_commands, build_prune_commands};
use crate::configuration::Configuration;

fn main() {
    setup_panic!();

    match inner_main() {
        Ok(()) => (),
        Err(error) => {
            error!("{}", error);
            exit(1);
        }
    }
}

fn inner_main() -> Result<()> {
    let matches = parse_command_line();
    setup_logging(matches.occurrences_of("verbosity"));

    let configuration_path = matches
            .value_of("configuration")
            .ok_or_else(|| anyhow!("Missing configuration argument"))?;
    debug!("Parsing configuration file {}", configuration_path);
    let file = File::open(configuration_path)
            .context("Could not open configuration file")?;
    let configuration: Configuration = serde_yaml::from_reader(file)?;

    let mut exit_statuses = Vec::new();

    let create_commands = build_create_commands(&configuration);
    info!("Creating backups to {} repositories", create_commands.len());
    for mut create_command in create_commands {
        debug!("{:?}", create_command);
        let exit_status = create_command
            .status()
            .context("Failed to create backup")?;
        exit_statuses.push(exit_status);
    }

    let prune_commands = build_prune_commands(&configuration);
    info!("Pruning backups to {} repositories", prune_commands.len());
    for mut prune_command in prune_commands {
        debug!("{:?}", prune_command);
        let exit_status = prune_command
            .status()
            .context("Failed to prune repository")?;
        exit_statuses.push(exit_status);
    }

    if exit_statuses.iter().all(|s| s.success()) {
        Ok(())
    } else {
        Err(anyhow!("Not all operations succeeded"))
    }
}

fn parse_command_line() -> ArgMatches<'static> {
    app_from_crate!()
        .arg(
            Arg::with_name("configuration")
                .short("c")
                .long("configuration")
                .value_name("PATH")
                .help("Path to configuration file")
                .takes_value(true)
                .required(true)
        )
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Verbosity level")
        )
        .get_matches()
}

fn setup_logging(verbosity: u64) {
    let default_log_filter = match verbosity {
        0 => "warn",
        1 => "info",
        2 => "info,borgbackup_helper=debug",
        3 | _ => "debug",
    };
    let filter = env_logger::Env::default().default_filter_or(default_log_filter);
    env_logger::Builder::from_env(filter)
        .format_timestamp(None)
        .init();
}
