// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use ::std::process::Command;

use ::glob::glob_with;
use ::glob::MatchOptions;

use crate::configuration::Configuration;

pub fn build_create_commands(configuration: &Configuration) -> Vec<Command> {
    let mut commands: Vec<Command> = Vec::new();

    for repository in configuration.location.repositories.iter() {

        let archive = format!("{}::{}", repository, configuration.storage.archive_name_format);

        let mut command = Command::new("borg");

        // Add common options
        //command.arg("");

        // Add command
        command.arg("create");

        // Add options
        // command.arg("--dry-run");
        command.arg("--verbose");
        command.arg("--progress");
        command.arg("--stats");

        for pattern in configuration.location.exclude_patterns.iter() {
            command.arg("--exclude");
            command.arg(pattern);
        }

        if configuration.location.exclude_caches {
            command.arg("--exclude-caches");
        }
        if let Some(filename) = configuration.location.exclude_if_present.as_ref().map(String::as_str) {
            command.arg("--exclude-if-present");
            command.arg(filename);
        }

        if configuration.location.one_file_system {
            command.arg("--one-file-system");
        }

        // Add archive
        command.arg(archive);

        // Add paths to backup
        let glob_match_options = MatchOptions {
            case_sensitive: false,
            require_literal_separator: true,
            require_literal_leading_dot: true,
        };
        for path in configuration.location.source_directories.iter() {
            for entry in glob_with(path, glob_match_options).expect("Failed to read glob pattern") {
                command.arg(entry.unwrap());
            }
        }

        commands.push(command);
    }

    commands
}

pub fn build_prune_commands(configuration: &Configuration) -> Vec<Command> {
    let mut commands: Vec<Command> = Vec::new();

    for repository in configuration.location.repositories.iter() {

        let mut command = Command::new("borg");

        // Add common options
        //command.arg("");

        // Add command
        command.arg("prune");

        // Add options
        // command.arg("--dry-run");
        command.arg("--stats");

        if let Some(interval) = configuration.retention.keep_within.as_ref().map(String::as_str) {
            command.arg("--keep-within");
            command.arg(interval.to_string());
        }

        if let Some(count) = configuration.retention.keep_secondly {
            command.arg("--keep-secondly");
            command.arg(count.to_string());
        }

        if let Some(count) = configuration.retention.keep_minutely {
            command.arg("--keep-minutely");
            command.arg(count.to_string());
        }

        if let Some(count) = configuration.retention.keep_hourly {
            command.arg("--keep-hourly");
            command.arg(count.to_string());
        }

        if let Some(count) = configuration.retention.keep_daily {
            command.arg("--keep-daily");
            command.arg(count.to_string());
        }

        if let Some(count) = configuration.retention.keep_weekly {
            command.arg("--keep-weekly");
            command.arg(count.to_string());
        }

        if let Some(count) = configuration.retention.keep_monthly {
            command.arg("--keep-monthly");
            command.arg(count.to_string());
        }

        if let Some(count) = configuration.retention.keep_yearly {
            command.arg("--keep-yearly");
            command.arg(count.to_string());
        }

        command.arg(repository);

        commands.push(command);
    }
    commands
}
