# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

CRATES="
{{ crates }}
"

inherit cargo
inherit check-reqs
inherit desktop

DESCRIPTION="A Rust application to simplify backups with BorgBackup"
HOMEPAGE="https://gitlab.com/claudiomattera/borgbackup-helper/"
SRC_URI="
    https://gitlab.com/claudiomattera/${PN}/-/archive/${PV}/${P}.tar.bz2
    $(cargo_crate_uris ${CRATES})
"
RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="x86 amd64"

DEPEND=""
RDEPEND=""
BDEPEND=""

CHECKREQS_DISK_BUILD="200M"

src_compile() {
    debug-print-function ${FUNCNAME} "$@"

    export CARGO_HOME="${ECARGO_HOME}"

    cargo build -v -j $(makeopts_jobs) $(usex debug "" --release) \
        || die "cargo build failed"
}

src_install() {
    debug-print-function ${FUNCNAME} "$@"

    cargo install --path . -j $(makeopts_jobs) --root="${D}/usr" $(usex debug --debug "") "$@" \
        || die "cargo install failed"
    rm -f "${D}/usr/.crates.toml"
    rm -f "${D}/usr/.crates2.json"

    [ -d "${S}/man" ] && doman "${S}/man"
}
